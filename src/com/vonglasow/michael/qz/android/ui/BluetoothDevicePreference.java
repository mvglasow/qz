/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.vonglasow.michael.qz.R;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.util.AttributeSet;

public class BluetoothDevicePreference extends ListPreference {

	public BluetoothDevicePreference(Context context) {
		super(context);
		updateDevices();
	}

	public BluetoothDevicePreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		updateDevices();
	}

	/**
	 * Regenerates the list of selectable devices.
	 */
	private void updateDevices() {
		Set<BluetoothDevice> devices = null;
		List<CharSequence> entries = new ArrayList<CharSequence>();
		List<CharSequence> values = new ArrayList<CharSequence>();

		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		if (btAdapter != null)
			devices = btAdapter.getBondedDevices();
		if (devices != null)
			for (BluetoothDevice device : devices) {
				entries.add(device.getName() + "\n" + device.getAddress());
				values.add(device.getAddress());
			}

		if (entries.isEmpty() || values.isEmpty()) {
			entries.add(getContext().getString(R.string.pref_bluetooth_dev_none));
			values.add("00:00:00:00:00:00");
		}

		setEntries(entries.toArray(new CharSequence[]{}));
		setEntryValues(values.toArray(new CharSequence[]{}));
	}

}
