/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.util;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class BluetoothManager {

	private static final String TAG = BluetoothManager.class.getName();

	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	/**
	 * @brief Instantiates a BluetoothSocket for the remote device and connects it.
	 * 
	 * This method implements a reflection fallback for cases in which
	 * {@link BluetoothDevice#createRfcommSocketToServiceRecord(UUID)} fails. See
	 * http://stackoverflow.com/questions/18657427/ioexception-read-failed-socket-might-closed-bluetooth-on-android-4-3/18786701#18786701
	 *
	 * @param dev The remote device to connect to
	 * @param silent Whether to suppress error logging
	 * @return The BluetoothSocket
	 * @throws IOException
	 */
	public static BluetoothSocket connect(BluetoothDevice dev, boolean silent) throws IOException {
		BluetoothSocket sock = null;
		BluetoothSocket sockFallback = null;

		Log.d(TAG, "Starting Bluetooth connection..");
		try {
			sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
			sock.connect();
		} catch (Exception e1) {
			String logMsg = "Error while establishing Bluetooth connection, falling back to reflection workaround";
			if (silent)
				Log.w(TAG, logMsg);
			else
				Log.w(TAG, logMsg, e1);
			Class<?> clazz = sock.getRemoteDevice().getClass();
			Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
			try {
				Method m = clazz.getMethod("createRfcommSocket", paramTypes);
				Object[] params = new Object[]{Integer.valueOf(1)};
				sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
				sockFallback.connect();
				sock = sockFallback;
			} catch (Exception e2) {
				if (!silent)
					Log.e(TAG, "Couldn't fallback while establishing Bluetooth connection.", e2);
				throw new IOException(e2.getMessage());
			}
		}
		return sock;
	}
}