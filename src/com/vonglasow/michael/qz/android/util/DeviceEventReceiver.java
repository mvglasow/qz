/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.util;

import com.vonglasow.michael.qz.android.core.QzApplication;
import com.vonglasow.michael.qz.android.core.TmcReceiver;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

/**
 * A broadcast receiver for various device events.
 * 
 * It is currently being used to attempt to connect to a Bluetooth TMC device upon certain trigger
 * events, such as connection to an external charger or another Bluetooth device (such as a
 * hands-free kit) connecting. Each indicates that the user might be in a car, where a Bluetooth
 * TMC dongle might be available.
 */
public class DeviceEventReceiver extends BroadcastReceiver {
	private String TAG = com.vonglasow.michael.qz.android.util.DeviceEventReceiver.class.getSimpleName();

	/* (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		if (Intent.ACTION_POWER_CONNECTED.equals(intent.getAction())
				|| Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())
				|| Intent.ACTION_LOCKED_BOOT_COMPLETED.equals(intent.getAction())
				|| BluetoothDevice.ACTION_ACL_CONNECTED.equals(intent.getAction())) {
			/* Find out whether we should initiate a Bluetooth connection */
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
			boolean autoConnect = preferences.getBoolean(Const.PREF_BLUETOOTH_AUTO, false);
			String tmcDevice = preferences.getString(Const.PREF_BLUETOOTH_DEV, null);

			if (autoConnect && (tmcDevice != null)) {
				/* Only try to connect if autoconnect is active and a BT device is configured */
				if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
						!= PackageManager.PERMISSION_GRANTED) {
					/* Log a warning and skip connection attempt if we have no permission */
					Log.w(TAG, "Location permission not granted. In order to use autoconnect, connect manually once and grant the permission.");
				} else {
					Log.i(TAG, String.format("Received %s, trying to connect", intent.getAction()));

					if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(intent.getAction())) {
						BluetoothDevice connectedDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
						if (tmcDevice.equals(connectedDevice.getAddress()))
							/* Abort if we were triggered by a connection to the configured BT device */
							return;
					}

					/* Create a new intent with the Bluetooth device and the silent option as extras */
					Intent outIntent = new Intent(Const.ACTION_BT_CONNECTION_REQUESTED, null, context, TmcReceiver.class);
					outIntent.putExtra(Const.EXTRA_DEVICE, tmcDevice);
					outIntent.putExtra(Const.EXTRA_SILENT, true);
					outIntent.putExtra(Const.EXTRA_KEEP_EXISTING_CONNECTION, true);

					/* Send intent to service */
					context.startService(outIntent);
				}
			}
		}
		/* In the end, always trigger a media scan on the log file if we have one */
		((QzApplication)(context.getApplicationContext())).runMediaScanner();
	}

}
