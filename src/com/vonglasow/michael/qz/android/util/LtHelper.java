/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import eu.jacquet80.rds.app.oda.tmc.TMC;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.util.Log;

/**
 * Helper class to initialize the location table database.
 */
public class LtHelper {

	/** The asset file which holds the database version. */
	private static final String DB_VERSION_FILE = "ltdb.version";

	/** The database files to extract from assets. */
	private static final String[] DB_ASSETS =
		{"ltdb.data", "ltdb.log", "ltdb.properties", "ltdb.script", DB_VERSION_FILE};

	private static final String TAG = "LtHelper";

	private static LtHelper instance = null;

	private Context context;

	public static synchronized LtHelper getInstance(Context context) {
		if (instance == null)
			instance = new LtHelper(context);
		return instance;
	}

	/**
	 * @brief Instantiates a new {@code LtHelper}.
	 * 
	 * @param context The context of the caller.
	 */
	private LtHelper(Context context) {
		super();
		this.context = context.getApplicationContext();
		extractLtDb();
		TMC.setDbUrl(Const.getLtDbUrl(this.context));
	}

	/**
	 * @brief Extracts a file asset from the application package
	 * 
	 * @param assets The AssetManager for the current context
	 * @param wrapper A ContextWrapper for the current context
	 * @param file The asset to be extracted
	 */
	private void extractAsset(AssetManager assets, ContextWrapper wrapper, String file) {
		/* No extensive error handling as most errors should never happen:
		 * We are copying only assets which we know exist in the package.
		 * The target files reside in a directory only this app has access to.
		 */
		InputStream in;
		try {
			in = assets.open(file);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		OutputStream out;
		try {
			out = new FileOutputStream(new File(wrapper.getFilesDir(), file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}

		byte[] buffer = new byte[65536];
		int len = 0;
		try {
			while ((len = in.read(buffer)) != -1)
				out.write(buffer, 0, len);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @brief Extracts the location table DB from assets if necessary
	 * 
	 * Before extracting the DB, this method checks if a DB has already been extracted and the
	 * version has not changed since. Unless both is the case, the DB will be extracted from
	 * assets.
	 */
	private void extractLtDb() {
		AssetManager assets = context.getAssets();
		ContextWrapper wrapper = new ContextWrapper(context);
		File versionFile = new File(wrapper.getFilesDir(), DB_VERSION_FILE);
		boolean hasCurrentDb = false;

		if (versionFile.exists()) {
			try {
				InputStream newIn = assets.open(DB_VERSION_FILE);
				byte[] newBuffer = new byte[108];
				newIn.read(newBuffer);
				newIn.close();
				String newVersion = new String(newBuffer);
				InputStream oldIn = new FileInputStream(versionFile);
				byte[] oldBuffer = new byte[108];
				oldIn.read(oldBuffer);
				oldIn.close();
				String oldVersion = new String(oldBuffer);
				hasCurrentDb = (newVersion.compareTo(oldVersion) <= 0);
			} catch (Exception e) {
				Log.w(TAG, "Could not read version file", e);
			}
		}

		if (!hasCurrentDb)
			for (String file : DB_ASSETS)
				extractAsset(assets, wrapper, file);
	}

}
