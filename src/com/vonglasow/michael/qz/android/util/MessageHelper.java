/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.IllegalFormatException;
import java.util.SimpleTimeZone;

import com.vonglasow.michael.qz.R;
import com.vonglasow.michael.qz.core.MessageWrapper;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import eu.jacquet80.rds.app.oda.AlertC.Event;
import eu.jacquet80.rds.app.oda.AlertC.InformationBlock;
import eu.jacquet80.rds.app.oda.AlertC.Message;
import eu.jacquet80.rds.app.oda.tmc.SupplementaryInfo;
import eu.jacquet80.rds.app.oda.tmc.TMC;

/**
 * Methods for working with TMC messages.
 */
public class MessageHelper {
	private static final String TAG = "MessageHelper";

	private static final long SECURITY_MASK =               0x40000L;
	private static final long WARNING_ACCIDENT_MASK =           0x4L;
	private static final long WARNING_SLIPPERY_MASK =   0x200002000L;
	private static final long WARNING_WIND_MASK =       0x800010000L;
	private static final long WARNING_ROADWORKS_MASK =        0x400L;
	private static final long WARNING_DANGER_MASK =       0x1C01808L;
	private static final long WARNING_DELAY_MASK =     0x2000080000L;
	private static final long WARNING_QUEUE_MASK =       0x80000003L;
	private static final long RESTRICTION_CLOSED_MASK =       0x1F0L;
	private static final long RESTRICTION_SIZE_MASK =     0x2000000L;
	private static final long INFO_CANCELLATION_MASK = 0x4000100000L;
	private static final long INFO_TEMPERATURE_MASK =  0x1000004000L;
	private static final long INFO_VISIBILITY_MASK =         0x8000L;
	private static final long INFO_EVENT_MASK =             0x20000L;
	private static final long INFO_TIME_MASK =             0x200000L;
	private static final long INFO_CARPOOL_MASK =             0x200L;
	private static final long INFO_PARKING_MASK =         0xC000000L;
	// private static final long INFO_INFO_MASK = 0x570000000L;


	/**
	 * Message class types.
	 * 
	 * The message class determines the nature of the message, reflected in an icon displayed to
	 * the user. Each message has exactly one message class, determined by the update classes of
	 * its constituent TMC events and an order of precedence.
	 */
	public static enum MessageClass {
		SECURITY,
		WARNING_ACCIDENT,
		WARNING_SLIPPERY,
		WARNING_WIND,
		WARNING_ROADWORKS,
		WARNING_DANGER,
		WARNING_DELAY,
		WARNING_QUEUE,
		RESTRICTION_CLOSED,
		RESTRICTION_SIZE,
		INFO_CANCELLATION,
		INFO_TEMPERATURE,
		INFO_VISIBILITY,
		INFO_EVENT,
		INFO_TIME,
		INFO_CARPOOL,
		INFO_PARKING,
		INFO_INFO,
	}

	/**
	 * @brief Returns a descriptive string for the detailed location of the message.
	 * 
	 * The detailed location typically refers to a stretch of road or a point on a road, which is
	 * the best approximation of the location of the event.
	 * 
	 * @param message
	 * @return
	 */
	public static String getDetailedLocationName(Message message) {
		String primaryName = message.getPrimaryName();
		String primaryJunctionNumber = message.getPrimaryJunctionNumber();
		if ((primaryJunctionNumber != null) && (!primaryJunctionNumber.isEmpty())) {
			if (primaryName == null)
				primaryName = primaryJunctionNumber;
			else
				primaryName = primaryName + " (" + primaryJunctionNumber + ")";
		}
		String secondaryName = message.getSecondaryName();
		String secondaryJunctionNumber = message.getSecondaryJunctionNumber();
		if ((secondaryJunctionNumber != null) && (!secondaryJunctionNumber.isEmpty())) {
			if (secondaryName == null)
				secondaryName = secondaryJunctionNumber;
			else
				secondaryName = secondaryName + " (" + secondaryJunctionNumber + ")";
		}
		if (primaryName != null) {
			if (secondaryName == null)
				return primaryName;
			else
				return String.format("%s %s %s",
						secondaryName,
						message.isBidirectional ? "↔" : "→",
								primaryName);
		} else
			return null;
	}

	/**
	 * @brief Returns a user-friendly string for the events in the message.
	 * 
	 * @param message
	 * @return
	 */
	public static String getEventText(Context context, Message message) {
		StringBuilder eventBuilder = new StringBuilder();
		for(InformationBlock ib : message.getInformationBlocks()) {
			// TODO destination

			for (Event event : ib.getEvents()) {
				if ((eventBuilder.length() > 0) && (eventBuilder.charAt(eventBuilder.length() - 1) != '?'))
					eventBuilder.append(". ");
				String eventText = getSingleEventText(context, event);
				eventText = eventText.substring(0, 1).toUpperCase() + eventText.substring(1);
				eventBuilder.append(eventText);

				for (SupplementaryInfo si : event.getSupplementaryInfo())
					eventBuilder.append(", " + context.getString(
							context.getResources().getIdentifier("si_" + si.code, "string", context.getPackageName())));
			}

			int length = ib.length;
			if (length != -1)
				try {
					eventBuilder.append(" ").append(getFormattedLength(context, length));
				} catch (IllegalFormatException e) {
					Log.w(TAG, "IllegalFormatException while parsing route length string, skipping");
					e.printStackTrace();
				}

			int speed = ib.speed;
			if(speed != -1)
				try {
					eventBuilder.append(", ").append(getFormattedSpeedLimit(context, speed));
				} catch (IllegalFormatException e) {
					Log.w(TAG, "IllegalFormatException while parsing speed limit string, skipping");
					e.printStackTrace();
				}

			// TODO diversion route
		}
		if (eventBuilder.charAt(eventBuilder.length() - 1) != '?')
			eventBuilder.append(".");

		if ((message.startTime > 0) || (message.stopTime > 0))
			eventBuilder.append(" ").append(getFormattedDuration(context, message)).append(".");

		return eventBuilder.toString();
	}

	/**
	 * @brief Returns the message class for a message.
	 * 
	 * The message class is determined based on the update classes of the constituent events of the
	 * message. Conflicts in determining the message class are resolved by applying the following
	 * order of precedence:
	 * <ul>
	 * <li>Security</li>
	 * <li>Warning</li>
	 * <li>Restriction</li>
	 * <li>Info</li>
	 * </ul>
	 * @param message
	 * @return
	 */
	public static MessageClass getMessageClass(Message message) {
		long updateClasses = 0;
		for (int evtCode : message.getEvents())
			updateClasses |= 1L << (TMC.getEvent(evtCode).updateClass - 1);

		if ((updateClasses & SECURITY_MASK) != 0)
			return MessageClass.SECURITY;
		if ((updateClasses & WARNING_ACCIDENT_MASK) != 0)
			return MessageClass.WARNING_ACCIDENT;
		if ((updateClasses & WARNING_SLIPPERY_MASK) != 0)
			return MessageClass.WARNING_SLIPPERY;
		if ((updateClasses & WARNING_WIND_MASK) != 0)
			return MessageClass.WARNING_WIND;
		if ((updateClasses & WARNING_ROADWORKS_MASK) != 0)
			return MessageClass.WARNING_ROADWORKS;
		if ((updateClasses & WARNING_DANGER_MASK) != 0)
			return MessageClass.WARNING_DANGER;
		if ((updateClasses & WARNING_DELAY_MASK) != 0)
			return MessageClass.WARNING_DELAY;
		if ((updateClasses & WARNING_QUEUE_MASK) != 0)
			return MessageClass.WARNING_QUEUE;
		if ((updateClasses & RESTRICTION_CLOSED_MASK) != 0)
			return MessageClass.RESTRICTION_CLOSED;
		if ((updateClasses & RESTRICTION_SIZE_MASK) != 0)
			return MessageClass.RESTRICTION_SIZE;
		if ((updateClasses & INFO_CANCELLATION_MASK) != 0)
			return MessageClass.INFO_CANCELLATION;
		if ((updateClasses & INFO_TEMPERATURE_MASK) != 0)
			return MessageClass.INFO_TEMPERATURE;
		if ((updateClasses & INFO_VISIBILITY_MASK) != 0)
			return MessageClass.INFO_VISIBILITY;
		if ((updateClasses & INFO_EVENT_MASK) != 0)
			return MessageClass.INFO_EVENT;
		if ((updateClasses & INFO_TIME_MASK) != 0)
			return MessageClass.INFO_TIME;
		if ((updateClasses & INFO_CARPOOL_MASK) != 0)
			return MessageClass.INFO_CARPOOL;
		if ((updateClasses & INFO_PARKING_MASK) != 0)
			return MessageClass.INFO_PARKING;
		/* fallback */
		return MessageClass.INFO_INFO;
	}

	/**
	 * @brief Formats time as a relative string.
	 * 
	 * The output can take the following forms:
	 * <ul>
	 * <li>10:15 (today)</li>
	 * <li>Tomorrow 11:00 (yesterday or tomorrow)</li>
	 * <li>Monday 11:00 (up to a week in the past or future)</li>
	 * <li>December 10 (same year; no time given here)</li>
	 * <li>February 8, 2020 (outside the current year)</li>
	 * </ul>
	 * 
	 * The format reflects the quantifier {@code tmcTime}. The time ranges for these values overlap, but they also
	 * carry precision information.
	 * 
	 * @param context The context
	 * @param date The date to format
	 * @return A formatted date
	 */
	public static String formatTime(Context context, Date date) {
		if (date == null)
			return null;

		Calendar thenCal = new GregorianCalendar();
		thenCal.setTime(date);
		Calendar nowCal = new GregorianCalendar();

		if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)
				&& thenCal.get(Calendar.MONTH) == nowCal.get(Calendar.MONTH)
				&& thenCal.get(Calendar.DAY_OF_MONTH) == nowCal.get(Calendar.DAY_OF_MONTH))
			/* today (time only) */
			return DateFormat.getTimeInstance(DateFormat.SHORT).format(date);
		else {
			Calendar thenDayCal = new GregorianCalendar(thenCal.get(Calendar.YEAR), thenCal.get(Calendar.MONTH),
					thenCal.get(Calendar.DAY_OF_MONTH));
			Calendar nowDayCal = new GregorianCalendar(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH),
					nowCal.get(Calendar.DAY_OF_MONTH));
			long dayDiff = (thenDayCal.getTimeInMillis() - nowDayCal.getTimeInMillis()) / 86400000;

			if (dayDiff == 1) {
				/* tomorrow (with time) */
				return String.format("%s, %s", context.getString(R.string.tomorrow),
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (dayDiff == -1) {
				/* yesterday (with time) */
				return String.format("%s, %s", context.getString(R.string.yesterday),
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (Math.abs(dayDiff) < 7) {
				/* within a week: day of week (with time) */
				return String.format("%s, %s", new SimpleDateFormat("EEEE").format(date),
						DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
			} else if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)) {
				/* same year: month, day */
				return DateUtils.formatDateTime(context, date.getTime(), DateUtils.FORMAT_SHOW_DATE);
			} else
				/* different year: month, day, year */
				return DateFormat.getDateInstance(DateFormat.LONG).format(date);
		}
	}

	/**
	 * @brief Formats time as a relative string.
	 * 
	 * The output can take the following forms:
	 * <ul>
	 * <li>10:15 (on the same day, 15-minute precision))</li>
	 * <li>Tomorrow 11:00 (hour precision)</li>
	 * <li>Monday 11:00 (up to four days in the future, hour precision)</li>
	 * <li>December 10 (up to a month in the future, day precision)</li>
	 * <li>mid-February (up to a year in the future, half-month precision)</li>
	 * <li>end of February (up to a year in the future, half-month precision)</li>
	 * </ul>
	 * 
	 * The format reflects the quantifier {@code tmcTime}. The time ranges for these values overlap, but they also
	 * carry precision information.
	 * 
	 * @param context The context
	 * @param date The date to format
	 * @param tmcTime The TMC time quantifier equivalent to {@code date}, used only for formatting
	 * @return A formatted date
	 */
	private static String formatTmcTime(Context context, Date date, int tmcTime) {
		if (date == null)
			return null;
		/*
		if (tmcTime >= 0 && tmcTime <= 95) {
			/* hh:mm (always today, can be in the past) //
			DateFormat format = DateFormat.getTimeInstance(DateFormat.SHORT);
			return format.format(date);
		} else
			*/
		if (tmcTime >= 0 && tmcTime <= 200) {
			/*
			 * hh:mm (on the day of the last update, can be in the past)
			 * or
			 * dddd hh:mm (up to four days from last update, can be in the past for old messages)
			 */
			String day;
			DateFormat format;
			Calendar thenCal = new GregorianCalendar();
			thenCal.setTime(date);
			Calendar nowCal = new GregorianCalendar();
			if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)
					&& thenCal.get(Calendar.MONTH) == nowCal.get(Calendar.MONTH)
					&& thenCal.get(Calendar.DAY_OF_MONTH) == nowCal.get(Calendar.DAY_OF_MONTH))
				/* date is today, display time (optionally prefixed with "today") */
				if (tmcTime <= 95)
					/* hh:mm today, display just the time */
					day = "%s";
				else
					/* dddd hh:mm, display the day */
					day = context.getString(R.string.today) + ", %s";
			else {
				if (nowCal.compareTo(thenCal) < 0) {
					/* date in the near future, use "tomorrow" or weekday */
					nowCal.add(Calendar.DAY_OF_MONTH, 1);
					if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)
							&& thenCal.get(Calendar.MONTH) == nowCal.get(Calendar.MONTH)
							&& thenCal.get(Calendar.DAY_OF_MONTH) == nowCal.get(Calendar.DAY_OF_MONTH))
						day = context.getString(R.string.tomorrow) + ", %s";
					else
						day = new SimpleDateFormat("EEEE").format(date) + ", %s";
				} else {
					/* date in the past, use "yesterday" with time or month/day without time */
					nowCal.add(Calendar.DAY_OF_MONTH, -1);
					if (thenCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)
							&& thenCal.get(Calendar.MONTH) == nowCal.get(Calendar.MONTH)
							&& thenCal.get(Calendar.DAY_OF_MONTH) == nowCal.get(Calendar.DAY_OF_MONTH))
						day = context.getString(R.string.yesterday) + ", %s";
					else
						return DateUtils.formatDateTime(context, date.getTime(), DateUtils.FORMAT_SHOW_DATE);
				}
			}
			format = DateFormat.getTimeInstance(DateFormat.SHORT);
			return String.format(day, format.format(date));

		} else if (tmcTime >= 201 && tmcTime <= 231) {
			/* mmmm dd (current or next month) */
			return DateUtils.formatDateTime(context, date.getTime(), DateUtils.FORMAT_SHOW_DATE);
		} else if (tmcTime >= 232 && tmcTime <= 255) {
			/* middle or end of month */
			String midOrEnd;
			Calendar cal = new GregorianCalendar(new SimpleTimeZone(0, ""));
			cal.clear();
			cal.setTime(date);
			/* get a value from the calendar to normalize fields */
			cal.get(Calendar.SECOND);
			/* in practice, we should only see 15 or 28–31 as days of month, +/- 1 day due to time zone differences */
			if (cal.get(Calendar.DAY_OF_MONTH) > 21)
				midOrEnd = context.getString(R.string.end);
			else
				midOrEnd = context.getString(R.string.mid);
			return String.format(midOrEnd, DateUtils.formatDateTime(context, date.getTime(),
					DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NO_MONTH_DAY));
		}
		return null;
	}

	/**
	 * @brief Formats the event duration as a string.
	 * 
	 * @param context The context
	 * @param message The message
	 * @return
	 */
	private static String getFormattedDuration(Context context, Message message) {
		Date startDate = MessageWrapper.resolveTime(message, message.startTime);
		String startText = formatTmcTime(context, startDate, message.startTime);
		String stopText = formatTmcTime(context, MessageWrapper.resolveTime(message, message.stopTime), message.stopTime);
		if ((startText != null) && (stopText != null)) {
			String duration = String.format(context.getString(R.string.duration), startText, stopText);
			return duration.substring(0, 1).toUpperCase() + duration.substring(1);
		} else if (startText != null) {
			Date now = new Date();
			if (startDate.before(now))
				return String.format(context.getString(R.string.since), startText);
			else
				return String.format(context.getString(R.string.from), startText);
		} else if (stopText != null) {
			return String.format(context.getString(R.string.until), stopText);
		} else
			return "";
	}

	/**
	 * @brief Formats the route length as a string.
	 * 
	 * If the length argument exceeds 100, the "for more than 100 km" string will be returned.
	 * 
	 * @param context The context
	 * @param length The length of the route affected, in km
	 * 
	 * @return
	 */
	private static String getFormattedLength(Context context, int length) {
		if (length > 100)
			return context.getString(R.string.q_km_max);
		return context.getResources().getQuantityString(context.getResources().getIdentifier(
				"q_km", "plurals", context.getPackageName()), length, length);
	}

	/**
	 * @brief Formats an event quantifier as a string.
	 * 
	 * Quantifiers cannot be of type 0 or 1, as these are processed directly by
	 * {@link #getSingleEventText(Context, Event)}.
	 * 
	 * @param context The context
	 * @param event The event from which the quantifier will be taken
	 * 
	 * @return
	 */
	private static String getFormattedQuantifier(Context context, Event event) {
		int q = event.quantifier;
		// q == 0 is the highest value, i.e. 2^5 (types 0-5) or 2^8 (types 6-12)
		if (q == 0) {
			q = event.tmcEvent.quantifierType <= 5 ? 32 : 256;
		}

		int resid = 0;
		double val = -1;

		switch (event.tmcEvent.quantifierType) {
		/* 0 and 1 are handled separately, not here */
		case 2:
			/* visibility */
			resid = context.getResources().getIdentifier(
					"q_visibility_m", "plurals", context.getPackageName());
			q *= 10;
			break;

		case 3:
			/* probability */
			resid = context.getResources().getIdentifier(
					"q_probability", "plurals", context.getPackageName());
			q = q - 1 * 5;
			break;

		case 4:
			/* speed limit */
			resid = context.getResources().getIdentifier(
					"q_km_h", "plurals", context.getPackageName());
			q *= 5;
			break;

		case 5:
			/* duration */
			if (q <= 10) {
				resid = context.getResources().getIdentifier(
						"q_duration_min", "plurals", context.getPackageName());
				q *= 5;
			} else {
				resid = context.getResources().getIdentifier(
						"q_duration_h", "plurals", context.getPackageName());
				if (q <=22)
					q -= 10;
				else
					q = (q - 20) * 6;
			}
			break;

		case 6:
			/* temperature */
			resid = context.getResources().getIdentifier(
					"q_temperature_celsius", "plurals", context.getPackageName());
			q -= 51;
			break;

		case 7:
			/* time */
			// TODO use current locale to format time
			return String.format("%02d:%02d",(q-1)/6, ((q-1)%6)*10);

		case 8:
			/* weight */
		case 9:
			/* dimension */
			val = q <= 100 ? q/10. : .5 * (q-80);
			if (event.tmcEvent.quantifierType == 8)
				resid = context.getResources().getIdentifier(
						"q_t", "plurals", context.getPackageName());
			else
				resid = context.getResources().getIdentifier(
						"q_m", "plurals", context.getPackageName());
			break;

		case 10:
			/* precipitation */
			resid = context.getResources().getIdentifier(
					"q_precipitation_mm", "plurals", context.getPackageName());
			break;

		case 11:
			/* frequency in MHz */
			resid = context.getResources().getIdentifier(
					"q_freq_mhz", "plurals", context.getPackageName());
			val = 87.5 + q / 10.;
			break;

		case 12:
			/* frequency in kHz */
			resid = context.getResources().getIdentifier(
					"q_freq_khz", "plurals", context.getPackageName());
			if (q < 16)
				q = q * 9 + 144;
			else
				q = q * 9 + 387;
			break;

		default:
			return "ILLEGAL";
		}

		if (val != -1)
			return context.getResources().getQuantityString(resid, (int) val, val);
		else
			return context.getResources().getQuantityString(resid, q, q);
	}

	/**
	 * @brief Format the speed limit as a string.
	 * 
	 * @param context The context
	 * @param speed The speed limit, in km/h
	 * 
	 * @return
	 */
	private static String getFormattedSpeedLimit(Context context, int speed) {
		return context.getResources().getQuantityString(context.getResources().getIdentifier(
				"q_maxspeed_km_h", "plurals", context.getPackageName()), speed, speed);
	}

	/**
	 * @brief Generates a formatted event description for a single event.
	 * 
	 * If the event has a quantifier, this method returns the quantifier string for the event
	 * with the quantifier parsed and inserted. Otherwise, the generic description is returned.
	 * 
	 * @param context The context
	 * @param event The event
	 * @return The message
	 */
	private static String getSingleEventText(Context context, Event event) {
		String text = "";
		if (event.quantifier != -1) {
			if (event.tmcEvent.quantifierType >= 2) {
				String q = getFormattedQuantifier(context, event);
				text = context.getString(context.getResources().getIdentifier(
						"event_" + event.tmcEvent.code + "_qn",
						"string", context.getPackageName()), q);
			} else {
				int q = (event.quantifier != 0) ? event.quantifier : 32;
				switch (event.tmcEvent.quantifierType) {
				case 0:
					if (q > 28)
						q = (q - 29) * 2 + 30;
					break;

				case 1:
					if (q > 14)
						q = (q - 12) * 50;
					else if (q > 4)
						q = (q - 4) * 10;
					break;
				}

				int resid = context.getResources().getIdentifier(
						"event_" + event.tmcEvent.code + "_qn", "plurals", context.getPackageName());
				if (resid != 0)
					text = context.getResources().getQuantityString(resid, q, q);
				else {
					resid = context.getResources().getIdentifier(
							"event_" + event.tmcEvent.code + "_qn", "string", context.getPackageName());
					if (resid != 0)
						text = context.getString(resid, Integer.toString(q));
				}
			}
		}
		if (text.isEmpty()) {
			text = context.getString(context.getResources().getIdentifier(
					"event_" + event.tmcEvent.code + "_q0", "string", context.getPackageName()));
		}
		return text;
	}
}
