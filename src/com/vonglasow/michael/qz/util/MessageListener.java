/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.util;

import java.util.Collection;

import com.vonglasow.michael.qz.core.MessageWrapper;

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * @brief Receives updates to the message cache.
 */
public interface MessageListener {
	/**
	 * @brief Processes a message update.
	 * 
	 * When a new message is received, it is passed as {@code added}.
	 * 
	 * When an update to a previously received message is received, the old message is referenced
	 * in {@code removed}. The new message passed as {@code added}, unless it is a cancellation
	 * message. One message may update multiple messages.
	 * 
	 * When a message expires, it is referenced in {@code removed}.
	 * 
	 * @param added A message which was newly added to, or updated in, the cache
	 * @param removed Messages which were removed from the cache because they expired, were
	 * replaced with a newer message or were canceled
	 */
	public void onUpdateReceived(MessageWrapper added, Collection<MessageWrapper> removed);
}
