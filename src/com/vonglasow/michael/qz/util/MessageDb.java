/*
 * Copyright © 2017–2019 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.vonglasow.michael.qz.core.MessageWrapper;
import com.vonglasow.michael.qz.core.WrappedMessageVisitor;

import eu.jacquet80.rds.app.oda.AlertC.Event;
import eu.jacquet80.rds.app.oda.AlertC.InformationBlock;
import eu.jacquet80.rds.app.oda.AlertC.Message;
import eu.jacquet80.rds.app.oda.AlertC.MessageBuilder;
import eu.jacquet80.rds.app.oda.tmc.SupplementaryInfo;
import eu.jacquet80.rds.app.oda.tmc.TMC;
import eu.jacquet80.rds.app.oda.tmc.TMCEvent;

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * Helper class to store AlertC messages in a database and recreate them
 */
public class MessageDb {
	/** 
	 * DB schema version for persistent storage of cached messages, incremented every time the schema changes.
	 * 
	 * You may wish to store this value in (or along with) your database and compare it on startup.
	 * If the versions do not match, reinitialize your local database. 
	 */
	public static final int DB_VERSION_MESSAGE_CACHE = 3;

	/** 
	 * SQL statements for persistent storage of TMC messages.
	 * 
	 * These are format strings into which table names must be inserted in the following order:
	 * Message, InformationBlock, Event, SupplementaryInformation, Diversion
	 */
	private static final String[] dbInitStmts = {
		// Table names inserted as parameters: Message (%1$s), InformationBlock (%2$s), Event (%3$s), SupplementaryInformation (%4$s), Diversion (%5$s), ReplacedMessage (%6$s)
		// Drop existing tables and indices
		"drop index if exists %2$s_message_index_idx;",
		"drop index if exists %3$s_informationBlock_index_idx;",
		"drop index if exists %4$s_event_index_idx;",
		"drop index if exists %5$s_informationBlock_index_idx;",
		"drop index if exists %6$s_message_idx;",
		"drop table if exists %6$s;",
		"drop table if exists %5$s;",
		"drop table if exists %4$s;",
		"drop table if exists %3$s;",
		"drop table if exists %2$s;",
		"drop table if exists %1$s;",
		// Message
		"create table %1$s(id integer identity primary key, msgid varchar(255), direction integer, extent integer, received timestamp(0), receivedTz varchar(255), date timestamp(0), timeZone varchar(255), cc integer, ltn integer, sid integer, interroad boolean, fcc integer, fltn integer, location integer, encrypted boolean, bidirectional boolean, durationType varchar(14), duration integer, startTime integer, stopTime integer, urgency varchar(7), nature varchar(8), spoken boolean, diversion boolean, updateCount integer);",
		// Information block
		"create table %2$s(id integer identity primary key, message integer, index integer, length integer, speed integer, destination integer, foreign key(message) references %1$s(id) on delete cascade);",
		"create index %2$s_message_index_idx ON %2$s(message, index);",
		// Event
		"create table %3$s(id integer identity primary key, informationBlock integer, index integer, code integer, sourceLocation integer, quantifier integer, foreign key(informationBlock) references %2$s(id) on delete cascade);",
		"create index %3$s_informationBlock_index_idx ON %3$s(informationBlock, index);",
		// Supplementary Information
		"create table %4$s(id integer identity primary key, event integer, index integer, code integer, foreign key(event) references %3$s(id) on delete cascade);",
		"create index %4$s_event_index_idx ON %4$s(event, index);",
		// Diversion
		"create table %5$s(id integer identity primary key, informationBlock integer, index integer, location integer, foreign key(informationBlock) references %2$s(id) on delete cascade);",
		"create index %5$s_informationBlock_index_idx ON %5$s(informationBlock, index);",
		// ReplacedMessage
		"create table %6$s(id integer identity primary key, message integer, replacedMsgid varchar(255), foreign key(message) references %1$s(id) on delete cascade);",
		"create index %6$s_message_idx ON %6$s(message);",
	};

	private MessageBuilder builder = new MessageBuilder();
	private MessageDbInfo dbInfo;
	private StoringVisitor storingVisitor = new StoringVisitor();

	/**
	 * @brief Initializes a database for persistent storage of TMC messages.
	 * 
	 * Any existing tables whose name matches any of the names specified in {@code dbInfo} will be
	 * dropped and recreated. Other system objects, such as indices, with names derived from the
	 * table names, will also be dropped and recreated. It is therefore recommended to avoid using
	 * these table names as part of system object names outside the schema created by this method.
	 * 
	 * This mechanism can also be used to upgrade a database after a schema change. Note that this
	 * will purge any stored TMC messages.
	 * 
	 * @param dbInfo Specifies the JDBC connection and table names to use
	 */
	public void initDb() {
		for (String stmtRaw: dbInitStmts) {
			try {
				String stmtFmt = String.format(stmtRaw,
						dbInfo.message,
						dbInfo.informationBlock,
						dbInfo.event,
						dbInfo.supplementaryInfo,
						dbInfo.diversion,
						dbInfo.replacedMessage);
				PreparedStatement stmt = dbInfo.connection.prepareStatement(stmtFmt);
				stmt.execute();
				stmt.close();
				dbInfo.connection.commit();
			} catch (SQLException e) {
				e.printStackTrace(System.err);
				return;
			}
		}
	}

	/**
	 * @brief Initializes a new storage interface.
	 * 
	 * @param connection A JDBC connection to the database in which messages are to be stored.
	 * Only HyperSQL is fully supported as a DBMS. Use other DBMSes at your own risk.
	 */
	public MessageDb(Connection connection) {
		this(connection, null, null, null, null, null, null);
	}

	/**
	 * @brief Initializes a new storage interface.
	 * 
	 * @param connection A JDBC connection to the database in which messages are to be stored.
	 * Only HyperSQL is fully supported as a DBMS. Use other DBMSes at your own risk.
	 * @param message The name of the table in which messages are stored; pass null to use the
	 * default name
	 * @param informationBlock The name of the table in which information blocks are stored; pass
	 * null to use the default name
	 * @param event The name of the table in which events are stored; pass null to use the default
	 * name
	 * @param supplementaryInfo The name of the table in which supplementary information items are
	 * stored; pass null to use the default name
	 * @param diversion The name of the table in which diversion locations are stored; pass null to
	 * use the default name
	 * @param replacedMessage The name of the table in which replaced message IDs are stored; pass null to use the
	 * default name
	 */
	public MessageDb(Connection connection, String message, String informationBlock, String event,
			String supplementaryInfo, String diversion, String replacedMessage) {
		this.dbInfo = new MessageDbInfo(connection);
		if (message != null)
			dbInfo.message = message;
		if (informationBlock != null)
			dbInfo.informationBlock = informationBlock;
		if (event != null)
			dbInfo.event = event;
		if (supplementaryInfo != null)
			dbInfo.supplementaryInfo = supplementaryInfo;
		if (diversion != null)
			dbInfo.diversion = diversion;
		if (replacedMessage != null)
			dbInfo.message = replacedMessage;
	}

	/**
	 * @brief Deletes this message from persistent storage
	 * 
	 * The {@code commit} argument has no effect if autocommit is enabled on the connection.
	 * 
	 * If autocommit is not enabled on the connection, the caller must either set {@code commit}
	 * to {@code true} or manually call {@link Connection#commit()} after this method returns.
	 * Multiple subsequent calls to this method can be made without committing. This can be
	 * useful for deleting multiple messages in bulk.
	 * 
	 * @param id The database ID of the message to delete
	 * @param commit Whether to commit after the operation, see above
	 * 
	 * @throws SQLException 
	 */
	public void delete(int id, boolean commit) throws SQLException {
		PreparedStatement stmt = dbInfo.connection.prepareStatement(String.format("delete from %s where id = ?", dbInfo.message));
		stmt.setInt(1, id);
		stmt.execute();
		stmt.close();
		if (commit && !dbInfo.connection.getAutoCommit())
			dbInfo.connection.commit();
	}

	/*
	 * @brief Returns a {@link ResultSet} of all records currently in the database
	 * 
	 * This method is useful for bulk creation of {@code Message} objects from an underlying
	 * database: obtain a result set with this method, then walk through it with
	 * {@link ResultSet#next()} and call {@link #Message(ResultSet, MessageDbInfo)} for each
	 * record returned.
	 * 
	 * The caller must close the {@code ResultSet} when it is done with it.
	 * 
	 * Note that, if autocommit is not enabled on the connection, the caller must also call
	 * {@link Connection#commit()} on the connection when it has finished processing the result.
	 * 
	 * @param dbInfo Describes the database in which the messages are stored
	 * 
	 * @return The messages
	 * 
	 * @throws SQLException
	 */
	/**
	 * @brief Reads the message database into main memory.
	 * 
	 * The {@code commit} argument has no effect if autocommit is enabled on the connection.
	 * 
	 * If autocommit is not enabled on the connection, the caller must either set {@code commit}
	 * to {@code true} or manually call {@link Connection#commit()} after this method returns.
	 * Multiple subsequent calls to this method can be made without committing. This can be
	 * useful for deleting multiple messages in bulk.
	 * 
	 * @param commit Whether to commit after the operation, see above
	 * 
	 * @return A map containing the messages as keys and their database IDs as values
	 * 
	 * @throws SQLException 
	 */
	public Map<MessageWrapper, Integer> readMessages(boolean commit, Set<String> idsInUse) throws SQLException {
		Map<MessageWrapper, Integer> res = new HashMap<MessageWrapper, Integer>();
		PreparedStatement stmt = dbInfo.connection.prepareStatement(String.format("select * from %s", dbInfo.message));
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			boolean startsNewInformationBlock = false;
			boolean bidirectional = true;
			int dbId = rset.getInt("id");
			builder.setServiceInfo(rset.getInt("cc"), rset.getInt("ltn"), rset.getInt("sid"),
					TimeZone.getTimeZone(rset.getString("timeZone")), rset.getBoolean("encrypted"));
			builder.setDirection(rset.getInt("direction"));
			builder.setExtent(rset.getInt("extent"));
			if (rset.getBoolean("interroad")) {
				int fcc = rset.getInt("fcc");
				int fltn = rset.getInt("fltn");
				int locationOrFlt = ((fcc & 0xF) << 6) | (fltn & 0x3F) | 0xFC00;
				builder.setLcid(locationOrFlt);
			}
			builder.setLcid(rset.getInt("location"));
			builder.setDate(new Date(rset.getTimestamp("date").getTime()));
			builder.setDiversion(rset.getBoolean("diversion"));

			PreparedStatement stmtRm = dbInfo.connection.prepareStatement(String.format("select * from %s where message = ?", dbInfo.replacedMessage));
			stmtRm.setInt(1, dbId);
			ResultSet rsetRm = stmtRm.executeQuery();
			Set<String> replacedMsgids = new HashSet<String>();
			while (rsetRm.next())
				replacedMsgids.add(rsetRm.getString("replacedMsgid"));
			rsetRm.close();
			stmtRm.close();

			PreparedStatement stmtI = dbInfo.connection.prepareStatement(String.format("select * from %s where message = ? order by index", dbInfo.informationBlock));
			stmtI.setInt(1, dbId);
			ResultSet rsetI = stmtI.executeQuery();
			if (rsetI.isAfterLast())
				throw new IllegalArgumentException("No information blocks for message found in database");
			while (rsetI.next()) {
				int dbIdI = rsetI.getInt("id");

				/* get events */
				PreparedStatement stmtE = dbInfo.connection.prepareStatement(String.format("select * from %s where informationBlock = ? order by index", dbInfo.event));
				stmtE.setInt(1, dbIdI);
				ResultSet rsetE = stmtE.executeQuery();
				if (rsetE.isAfterLast())
					throw new IllegalArgumentException("No events for information block found in database");
				while (rsetE.next()) {
					int dbIdE = rsetE.getInt("id");
					if (startsNewInformationBlock)
						builder.addInformationBlock(rsetE.getInt("code"));
					else
						builder.addEvent(rsetE.getInt("code"));
					bidirectional &= TMC.getEvent(rsetE.getInt("code")).bidirectional;
					builder.setSourceLocation(rsetE.getInt("sourceLocation"));
					builder.setQuantifier(rsetE.getInt("quantifier"));

					/* get supplementary information */
					PreparedStatement stmtSi = dbInfo.connection.prepareStatement(String.format("select * from %s where event = ? order by index", dbInfo.supplementaryInfo));
					stmtSi.setInt(1, dbIdE);
					ResultSet rsetSi = stmtSi.executeQuery();
					while (rsetSi.next())
						builder.addSupplementaryInformation(rsetSi.getInt("code"));
					rsetSi.close();
					stmtSi.close();
				}
				rsetE.close();
				stmtE.close();

				startsNewInformationBlock = true;

				builder.setLength(rsetI.getInt("length"));
				builder.setSpeed(rsetI.getInt("speed"));
				builder.setDestinationLcid(rsetI.getInt("destination"));

				/* get diversion entries */
				PreparedStatement stmtD = dbInfo.connection.prepareStatement(String.format("select * from %s where informationBlock = ? order by index", dbInfo.diversion));
				stmtD.setInt(1, dbIdI);
				ResultSet rsetD = stmtD.executeQuery();
				while (rsetD.next()) {
					builder.addDiversionLcid(rsetD.getInt("location"));
				}
				rsetD.close();
				stmtD.close();
			}

			rsetI.close();
			stmtI.close();

			builder.setDuration(rset.getInt("duration"));
			if (bidirectional != rset.getBoolean("bidirectional"))
				builder.reverseDirectionality();
			builder.setStartTime(rset.getInt("startTime"));
			builder.setStopTime(rset.getInt("stopTime"));
			builder.setUrgency(TMCEvent.EventUrgency.valueOf(rset.getString("urgency")));
			builder.setNature(TMCEvent.EventNature.valueOf(rset.getString("nature")));
			builder.setDurationType(TMCEvent.EventDurationType.valueOf(rset.getString("durationType")));
			if (rset.getBoolean("spoken"))
				builder.reverseSpoken();
			builder.setUpdateCount(rset.getInt("updateCount"));

			String msgid = rset.getString("msgid");
			Timestamp received = rset.getTimestamp("date");
			String receivedTz = rset.getString("receivedTz");
			Message message = builder.build();
			MessageWrapper wrapper;

			if ((msgid != null) && (received != null) && (receivedTz != null))
				wrapper = new MessageWrapper(message, replacedMsgids, msgid, new Date(received.getTime()), TimeZone.getTimeZone(receivedTz));
			else
				/*
				 * This may cause duplicates if a message with the same ID as the auto-generated one is read in later,
				 * but this scenario is unlikely to occur.
				 */
				wrapper = new MessageWrapper(message, replacedMsgids, idsInUse);

			idsInUse.add(wrapper.id);
			idsInUse.addAll(replacedMsgids);
			res.put(wrapper, dbId);
		}

		stmt.close();
		rset.close();

		if (commit && !dbInfo.connection.getAutoCommit())
			dbInfo.connection.commit();

		return res;
	}

	/**
	 * @brief Writes a TMC/ALERT-C message to persistent storage.
	 * 
	 * 
	 * The {@code commit} argument has no effect if autocommit is enabled on the connection.
	 * 
	 * If autocommit is not enabled on the connection, the caller must either set {@code commit}
	 * to {@code true} or manually call {@link Connection#commit()} after this method returns.
	 * Multiple subsequent calls to this method can be made without committing. This can be
	 * useful for storing multiple messages in bulk.
	 * 
	 * @param wrapper The wrapper around the message to store
	 * @param commit Whether to commit after the operation, see above
	 * 
	 * @return The ID of the message in the database.
	 * 
	 * @throws SQLException 
	 */
	public int store(MessageWrapper wrapper, boolean commit) throws SQLException {
		wrapper.accept(storingVisitor, true);
		PreparedStatement stmt = dbInfo.connection.prepareStatement(
				String.format("update %s set msgid = ?, received = ?, receivedTz = ? where id = ?", dbInfo.message));
		stmt.setString(1, wrapper.id);
		stmt.setTimestamp(2, new Timestamp(wrapper.received.getTime()));
		stmt.setString(3, wrapper.receivedTz.getID());
		stmt.setInt(4, storingVisitor.messageId);

		if (commit && !dbInfo.connection.getAutoCommit())
			dbInfo.connection.commit();
		return storingVisitor.messageId;
	}

	private class StoringVisitor implements WrappedMessageVisitor {
		private int messageId = -1;
		private int ibId = -1;
		private int ibIndex;
		private int evIndex;

		/**
		 * @brief Writes a TMC/ALERT-C message to persistent storage.
		 * 
		 * The caller is responsible for ensuring that {@link Connection#commit()} is called after
		 * this method returns.
		 * Multiple subsequent calls to this method can be made without committing. This can be
		 * useful for storing multiple messages in bulk.
		 * 
		 * @param wrapper The wrapper for the message to store
		 */
		@Override
		public void visit(MessageWrapper wrapper) {
			PreparedStatement stmt;

			if (messageId == -1)
				throw new RuntimeException("Failed to obtain ID for Message, has it been inserted yet?");
			/* add mappings */
			if (wrapper.replacedIds != null)
				for (String replacedId : wrapper.replacedIds) {
					try {
						stmt = dbInfo.connection.prepareStatement(String.format(
								"insert into %s (message, replacedMsgid) values(?, ?)",
								dbInfo.replacedMessage),
								Statement.RETURN_GENERATED_KEYS);
						stmt.setInt(1, messageId);
						stmt.setString(2, replacedId);
						int rows = stmt.executeUpdate();
						if (rows == 0)
							throw new SQLException("Insert ReplacedMessage failed");
					} catch (SQLException e) {
						throw new RuntimeException(e);
					}
					try {
						stmt.close();
					} catch (SQLException e) {
						// NOP
					}
				}
		}

		/**
		 * @brief Writes a TMC/ALERT-C message to persistent storage.
		 * 
		 * @param message The message to store
		 */
		@Override
		public void visit(Message message) {
			this.messageId = -1;
			this.ibIndex = 0;
			PreparedStatement stmt;
			ResultSet keys;

			try {
				stmt = dbInfo.connection.prepareStatement(String.format(
						"insert into %s (direction, extent, date, timeZone, cc, ltn, sid, interroad, fcc, fltn, location, encrypted, bidirectional, durationType, duration, startTime, stopTime, urgency, nature, spoken, diversion, updateCount) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
						dbInfo.message),
						Statement.RETURN_GENERATED_KEYS);

				stmt.setInt(1, message.direction);
				stmt.setInt(2, message.extent);
				stmt.setTimestamp(3, new Timestamp(message.getTimestamp().getTime()));
				stmt.setString(4, message.timeZone.getID());
				stmt.setInt(5, message.cc);
				stmt.setInt(6, message.getLocationTableNumber());
				stmt.setInt(7, message.getSid());
				stmt.setBoolean(8, message.interroad);
				stmt.setInt(9, message.fcc);
				stmt.setInt(10, message.getForeignLocationTableNumber());
				stmt.setInt(11, message.lcid);
				stmt.setBoolean(12, message.isEncrypted);
				stmt.setBoolean(13, message.isBidirectional);
				stmt.setString(14, message.durationType.name());
				stmt.setInt(15, message.duration);
				stmt.setInt(16, message.startTime);
				stmt.setInt(17, message.stopTime);
				stmt.setString(18, message.urgency.name());
				stmt.setString(19, message.nature.name());
				stmt.setBoolean(20, message.spoken);
				stmt.setBoolean(21, message.hasDiversion);
				stmt.setInt(22, message.getUpdateCount());

				int rows = stmt.executeUpdate();
				if (rows == 0)
					throw new SQLException("Insert Message failed");

				keys = stmt.getGeneratedKeys();
				if (keys.next())
					messageId = keys.getInt(1);
				else
					throw new SQLException("Failed to obtain ID for newly inserted Message");
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			try {
				keys.close();
				stmt.close();
			} catch (SQLException e) {
				// NOP
			}
		}

		/**
		 * @brief Writes an information block to persistent storage.
		 * 
		 * @param informationBlock The information block to store
		 */
		@Override
		public void visit(InformationBlock informationBlock) {
			this.ibId = -1;
			this.evIndex = 0;
			PreparedStatement stmt;
			ResultSet keys;
			PreparedStatement stmtD;

			try {
				stmt = dbInfo.connection.prepareStatement(String.format(
						"insert into %s (message, index, length, speed, destination) values(?, ?, ?, ?, ?)",
						dbInfo.informationBlock),
						Statement.RETURN_GENERATED_KEYS);

				stmt.setInt(1, messageId);
				stmt.setInt(2, this.ibIndex);
				stmt.setInt(3, informationBlock.length);
				stmt.setInt(4, informationBlock.speed);
				stmt.setInt(5, informationBlock.destinationLcid);

				int rows = stmt.executeUpdate();
				if (rows == 0)
					throw new SQLException("Insert InformationBlock failed");

				keys = stmt.getGeneratedKeys();
				if (keys.next())
					ibId = keys.getInt(1);
				else
					throw new SQLException("Failed to obtain ID for newly inserted InformationBlock");
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			try {
				keys.close();
				stmt.close();
			} catch (SQLException e) {
				// NOP
			}

			/* store diversions */
			int i = 0;
			for (Integer div : informationBlock.getDiversionLcids()) {
				try {
					stmtD = dbInfo.connection.prepareStatement(String.format(
							"insert into %s (informationBlock, index, location) values(?, ?, ?)",
							dbInfo.diversion),
							Statement.RETURN_GENERATED_KEYS);

					stmtD.setInt(1, ibId);
					stmtD.setInt(2, i);
					stmtD.setInt(3, div);

					int rowsD = stmtD.executeUpdate();
					if (rowsD == 0)
						throw new SQLException("Insert Diversion failed");

					/* No need to get the ID because we're not referencing diversions */
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
				try {
					stmtD.close();
				} catch (SQLException e) {
					// NOP
				}
				i++;
			}

			this.ibIndex++;
		}

		/**
		 * @brief Writes an event to persistent storage.
		 * 
		 * @param event The event to store
		 */
		@Override
		public void visit(Event event) {
			int evId;
			PreparedStatement stmt;
			ResultSet keys;
			PreparedStatement stmtSi;

			try {
				stmt = dbInfo.connection.prepareStatement(String.format(
						"insert into %s (informationBlock, index, code, sourceLocation, quantifier) values(?, ?, ?, ?, ?)",
						dbInfo.event),
						Statement.RETURN_GENERATED_KEYS);

				stmt.setInt(1, ibId);
				stmt.setInt(2, evIndex);
				stmt.setInt(3, event.tmcEvent.code);
				stmt.setInt(4, event.sourceLcid);
				stmt.setInt(5, event.quantifier);

				int rows = stmt.executeUpdate();
				if (rows == 0)
					throw new SQLException("Insert Event failed");

				keys = stmt.getGeneratedKeys();
				if (keys.next())
					evId = keys.getInt(1);
				else
					throw new SQLException("Failed to obtain ID for newly inserted Event");
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			try {
				keys.close();
				stmt.close();
			} catch (SQLException e) {
				// NOP
			}

			/* store SupplementaryInfo */
			int i = 0;
			for (SupplementaryInfo si : event.getSupplementaryInfo()) {
				try {
					stmtSi = dbInfo.connection.prepareStatement(String.format(
							"insert into %s (event, index, code) values(?, ?, ?)",
							dbInfo.supplementaryInfo),
							Statement.RETURN_GENERATED_KEYS);

					stmtSi.setInt(1, evId);
					stmtSi.setInt(2, i);
					stmtSi.setInt(3, si.code);

					int rowsSi = stmtSi.executeUpdate();
					if (rowsSi == 0)
						throw new SQLException("Insert SupplementaryInfo failed");

					/* No need to get the ID because we're not referencing SupplementaryInfo items */
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
				try {
					stmtSi.close();
				} catch (SQLException e) {
					// NOP
				}
				i++;
			}

			this.evIndex++;
		}
	}


	/**
	 * Describes the database used for persistent storage of TMC messages.
	 * 
	 * When an instance of this class is created, its table name members have default values. You
	 * can change them as you wish. Bear in mind that input is not sanitized, thus be sure to
	 * either set these variables in a way that you control their content (as with a fixed string)
	 * or sanitize it.
	 */
	public static class MessageDbInfo {
		/** The database connection. */
		public Connection connection;
		/** The name of the table in which TMC messages are stored. */
		public String message = "Message";
		/** The name of the table in which information blocks of TMC messages are stored. */
		public String informationBlock = "InformationBlock";
		/** The name of the table in which the events of each TMC information block are stored. */
		public String event = "Event";
		/** The name of the table in which supplementary info for TMC events is stored. */
		public String supplementaryInfo = "SupplementaryInfo";
		/** The name of the table in which diversion routes for TMC information blocks are stored. */
		public String diversion = "Diversion";
		/** The name of the table in which replaced message IDs are stored. */
		public String replacedMessage = "ReplacedMessage";

		/**
		 * @brief Creates a new {@code MessageDbInfo} instance.
		 * 
		 * @param connection A JDBC connection to the database in which messages are to be stored.
		 * Only HyperSQL is fully supported as a DBMS. Use other DBMSes at your own risk.
		 */
		public MessageDbInfo(Connection connection) {
			this.connection = connection;
		}
	}
}
