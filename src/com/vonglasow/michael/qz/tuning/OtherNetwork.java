/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.tuning;

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * @brief Describes an Other Network (ON). 
 *
 * Instances of this class are immutable.
 */
public class OtherNetwork {
	/**
	 * The minimum frequency that can be represented in TMC tuning information.
	 * 
	 * Frequencies below (and including) {@code MIN_FREQ} are invalid.
	 */
	public static final float MIN_FREQ = 87.5f;

	public final int tnPi;
	public final Float tnFreq;
	public final int onPi;
	public final float onFreq;

	/**
	 * @brief Instantiates an OtherNetwork.
	 * 
	 * @param tnPi Program Identification (PI) of transmitting station (TN—Tuned Network)
	 * @param tnFreq Frequency of transmitting station (TN—Tuned Network), can be null
	 * @param onPi Program Identification (PI) of Other Network
	 * @param onFreq Frequency of Other Network (AF—Alternate Frequency)
	 */
	public OtherNetwork(int tnPi, Float tnFreq, int onPi, float onFreq) {
		this.tnPi = tnPi;
		this.tnFreq = ((tnFreq == null) || (tnFreq > MIN_FREQ)) ? tnFreq : null;
		this.onPi = onPi;
		this.onFreq = onFreq;
	}

	/**
	 * @brief Convenience constructor for an OtherNetwork which is not bound to a Tuned Network frequency.
	 * 
	 * @param tnPi Program Identification (PI) of transmitting station (TN—Tuned Network)
	 * @param onPi Program Identification (PI) of Other Network
	 * @param onFreq Frequency of Other Network (AF—Alternate Frequency)
	 */
	public OtherNetwork(int tnPi, int onPi, float onFreq) {
		this(tnPi, null, onPi, onFreq);
	}

	@Override
	public boolean equals(Object obj) {
		return ((obj instanceof OtherNetwork)
				&& (this.tnPi == ((OtherNetwork) obj).tnPi)
				&& (this.tnFreq == null ? ((OtherNetwork) obj).tnFreq == null : this.tnFreq.equals(((OtherNetwork) obj).tnFreq))
				&& (this.onPi == ((OtherNetwork) obj).onPi)
				&& (this.onFreq == ((OtherNetwork) obj).onFreq));
	}

	@Override
	public int hashCode() {
		return tnPi * 29791 
				+ ((tnFreq == null) ? 0 : ((int)(tnFreq * 9610))) 
				+ onPi * 31
				+ (int)(onFreq * 10);
	}
}