Terms of use and limitation of liability

The user undertakes to follow all special and general conditions and to
appropriately indicate the source of information and copyright information on
all products using the services and information. DARS d. d., Ministry of
infrastructure, RTV SLO and other service and information providers shall in no
case be liable for any special, unintentional, indirect or consequential damage
(including (without limitation) the damage for loss of business profit, business
interruption, loss of business information, personal injuries, loss of privacy,
failure to perform any obligation (also in good faith or with due diligence),
negligence or any other material or other damage) arising out of or in any
connection with the use or inability to use the services and information, and
shall not incur any legal consequences due to service and information use
errors.
